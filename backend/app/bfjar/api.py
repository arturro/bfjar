from django.urls import path
from rest_framework import routers

from bfjar.apps.jar.views import CurrencyViewSet, JarViewSet, TransferViewSet, TransferFundView

router = routers.DefaultRouter()
router.register(r'currency', CurrencyViewSet)
router.register(r'jar', JarViewSet)
router.register(r'transfer', TransferViewSet)

urlpatterns = [
    path('transfer-funds/', TransferFundView.as_view(), name='api-transfer-funds'),
]

urlpatterns += router.urls
