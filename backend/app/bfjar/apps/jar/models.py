from decimal import Decimal
import logging

from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _
from .exceptions import InvalidCurrency, InsufficientFund

logger = logging.getLogger(__name__)


class Currency(models.Model):
    code = models.CharField(_('code'), max_length=3, unique=True)

    class Meta:
        verbose_name = _(u'Currency')
        verbose_name_plural = _(u'Currencies')

    def __str__(self):
        return self.code


class Jar(models.Model):
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='jar_currency')
    balance = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal(0))
    created_date = models.DateTimeField(_(u'created date'), auto_now_add=True)
    name = models.CharField(_(u'name'), max_length=50, default='')

    class Meta:
        verbose_name = _(u'Jar')
        verbose_name_plural = _(u'Jars')

    def __str__(self):
        return "Jar: {} balance: {} {}".format(self.pk, self.currency, self.balance)


# TransferManager - czy gdzie to wrzucic?
class TransferManager(models.Manager):
    def transfer_fund(self, to_jar, currency, amount, name, from_jar=None):
        with transaction.atomic():
            if to_jar.balance + amount < Decimal(0):
                raise InsufficientFund()
            to_jar.balance += amount
            to_jar.save()

            if from_jar:
                if from_jar.balance - amount < Decimal(0):
                    raise InsufficientFund()
                from_jar.balance -= amount
                from_jar.save()

            transfer = Transfer(
                to_jar=to_jar,
                currency=currency,
                amount=amount,
                name=name,
                from_jar=from_jar
            )
            transfer.save()
            return transfer


class Transfer(models.Model):
    from_jar = models.ForeignKey(
        Jar, on_delete=models.CASCADE, blank=True, null=True, related_name='transfer_from_jar'
    )
    to_jar = models.ForeignKey(
        Jar, on_delete=models.CASCADE, related_name='transfer_to_jar'
    )
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, related_name='transfer_currency')
    amount = models.DecimalField(max_digits=6, decimal_places=2, default=Decimal(0))
    created_date = models.DateTimeField(_(u'created date'), auto_now_add=True)
    name = models.CharField(_(u'name'), max_length=50, default='')

    class Meta:
        verbose_name = _(u'Transfer')
        verbose_name_plural = _(u'Transfers')

    def __str__(self):
        return "from: {} to: {} amount: {} {}".format(self.from_jar, self.to_jar, self.currency, self.amount)

    def save(self, *args, **kwargs):
        # Check Transfer currency.
        if self.to_jar.currency != self.currency:
            raise InvalidCurrency(
                "Transfer in different currency not allowed to.jar.currency: {}  currency: {}".format(
                    self.to_jar.currency, self.currency))
        if self.from_jar and self.from_jar.currency != self.currency:
            raise InvalidCurrency(
                "Transfer in different currency not allowed from_jar.currency: {}  currency: {}".format(
                    self.from_jar.currency, self.currency))
        super().save(*args, **kwargs)

    objects = TransferManager()
