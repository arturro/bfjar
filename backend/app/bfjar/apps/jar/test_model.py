from decimal import Decimal
from django.test import TestCase

from .constants import CURRENCY_CODE_EUR, CURRENCY_CODE_PLN
from .models import Currency, Jar, Transfer
from .exceptions import InvalidCurrency, InsufficientFund


class CurrencyTestCase(TestCase):
    def setUp(self):
        pass

    def test_dummy_create_object(self):
        currency_symbol = 'PLNa'
        currency = Currency.objects.create(code=currency_symbol)
        self.assertEqual(currency.code, format(currency_symbol))


class TransferTestCase(TestCase):
    def setUp(self):
        self.currency_pln = Currency.objects.create(code=CURRENCY_CODE_PLN)
        self.currency_eur = Currency.objects.create(code=CURRENCY_CODE_EUR)
        self.jar_pln_1 = Jar.objects.create(
            currency=self.currency_pln,
            balance=Decimal(0),
            name='jar_pln_1'
        )
        self.jar_pln_2 = Jar.objects.create(
            currency=self.currency_pln,
            balance=Decimal(0),
            name='jar_pln_2'
        )
        self.jar_eur_1 = Jar.objects.create(
            currency=self.currency_pln,
            balance=Decimal(0),
            name='jar_eur_1'
        )

    def test_transfer_funds_add_amount_should_return_obj(self):
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_pln,
            'amount': Decimal(10),
            'name': 'test 01'
        }
        transfer = Transfer.objects.transfer_fund(**transfer_data)
        self.assertTrue(transfer.pk)

    def test_transfer_funds_remove_amount_balance_enough_should_return_obj(self):
        transfer_data_1 = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_pln,
            'amount': Decimal(10),
            'name': 'test 01'
        }
        transfer_1 = Transfer.objects.transfer_fund(**transfer_data_1)
        transfer_data_2 = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_pln,
            'amount': Decimal(-10),
            'name': 'test 01'
        }
        transfer_2 = Transfer.objects.transfer_fund(**transfer_data_2)
        self.assertTrue(transfer_2.pk)

    def test_transfer_funds_add_amount_with_diff_currency_should_raise_exception(self):
        transfer_data_1 = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_pln,
            'amount': Decimal(10),
            'name': 'test 01'
        }
        transfer_1 = Transfer.objects.transfer_fund(**transfer_data_1)
        transfer_data_2 = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_eur,
            'amount': Decimal(10),
            'name': 'test 01'
        }
        with self.assertRaises(InvalidCurrency) as e:
            transfer_2 = Transfer.objects.transfer_fund(**transfer_data_2)

    def test_transfer_funds_remove_amount_balance_not_enough_should_raise_exception(self):
        transfer_data_1 = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_pln,
            'amount': Decimal(10),
            'name': 'test 01'
        }
        transfer_1 = Transfer.objects.transfer_fund(**transfer_data_1)
        transfer_data_2 = {
            'from_jar': None,
            'to_jar': self.jar_pln_1,
            'currency': self.currency_pln,
            'amount': Decimal(-15),
            'name': 'test 01'
        }
        with self.assertRaises(InsufficientFund) as e:
            transfer_2 = Transfer.objects.transfer_fund(**transfer_data_2)
