class JarException(Exception):
    """Base class for Jar exceptions"""
    pass


class InvalidCurrency(JarException):
    """Invalid currency for from_jar or to_jar"""
    pass


class InsufficientFund(JarException):
    """insufficient fund"""
    pass
    # TODO add msg to exception
