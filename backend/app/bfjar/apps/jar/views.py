import logging
from decimal import Decimal

from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from rest_framework import generics
from rest_framework import viewsets
from rest_framework import status
from rest_framework import serializers

from rest_framework.response import Response

from .exceptions import JarException
from .models import Jar, Currency, Transfer
from .serializers import CurrencySerializer, JarSerializer, TransferSerializer, TransferFundSerializer

logger = logging.getLogger(__name__)


class CurrencyViewSet(viewsets.ModelViewSet):
    """
    API endpoint with currency
    """
    queryset = Currency.objects.all()
    serializer_class = CurrencySerializer
    permission_classes = []


class JarViewSet(viewsets.ModelViewSet):
    """
    API endpoint with user's jars
    """
    queryset = Jar.objects.all()
    serializer_class = JarSerializer
    permission_classes = []


class TransferViewSet(viewsets.ModelViewSet):
    """
    API endpoint with history of transfers from/to user's jars
    """
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
    permission_classes = []
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    filterset_fields = ('from_jar', 'to_jar')
    ordering_fields = ['amount', 'created_date', 'name']


class TransferFundView(generics.CreateAPIView):
    queryset = Transfer.objects.all()
    serializer_class = TransferFundSerializer

    def create(self, request, *args, **kwargs):
        if request.data['from_jar']:
            try:
                from_jar = Jar.objects.get(pk=request.data['from_jar'])
            except Jar.DoesNotExist:
                return Response(
                    'from_jar DoesNotExist: {}'.format(request.data['from_jar']), status=status.HTTP_404_NOT_FOUND)
        else:
            from_jar = None
        try:
            to_jar = Jar.objects.get(pk=request.data['to_jar'])
        except Jar.DoesNotExist:
            return Response(
                'to_jar DoesNotExist: {}'.format(request.data['to_jar']), status=status.HTTP_404_NOT_FOUND)
        try:
            currency = Currency.objects.get(code=request.data['currency'])
        except Currency.DoesNotExist:
            return Response(
                'currency DoesNotExist: {}'.format(request.data['currency']), status=status.HTTP_404_NOT_FOUND)
        amount = request.data['amount']
        name = request.data['name']
        transfer_data = {
            'from_jar': from_jar,
            'to_jar': to_jar,
            'currency': currency,
            'amount': Decimal(amount),
            'name': name,
        }
        try:
            transfer = Transfer.objects.transfer_fund(**transfer_data)
            return Response({'created_transfer': transfer.pk})
        except JarException as e:
            raise serializers.ValidationError("There was a problem!")  # TODO
        except Exception as e:
            raise serializers.ValidationError("There was a problem!")
