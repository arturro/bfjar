from django.apps import AppConfig


class JarConfig(AppConfig):
    name = 'bfjar.apps.jar'
