from django.contrib import admin

from .models import Currency, Jar, Transfer

admin.site.register(Currency)
admin.site.register(Jar)
admin.site.register(Transfer)
