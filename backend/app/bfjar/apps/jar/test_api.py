from decimal import Decimal

from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from .constants import CURRENCY_CODE_EUR, CURRENCY_CODE_PLN
from .models import Currency, Jar


class JarTestCase(TestCase):
    def setUp(self):
        self.currency_pln = Currency.objects.create(code=CURRENCY_CODE_PLN)
        self.currency_eur = Currency.objects.create(code=CURRENCY_CODE_EUR)
        self.jar_pln_1 = Jar.objects.create(
            currency=self.currency_pln,
            balance=Decimal(0),
            name='jar_pln_1'
        )
        self.jar_pln_2 = Jar.objects.create(
            currency=self.currency_pln,
            balance=Decimal(20),
            name='jar_pln_2'
        )
        self.jar_eur_1 = Jar.objects.create(
            currency=self.currency_eur,
            balance=Decimal(20),
            name='jar_eur_1'
        )

    def test_add_fund_should_return_200(self):
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_remove_fund_enough_balance_should_return_200(self):
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(-10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_remove_fund_enough_balance_should_return_500(self):
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(-20),
            'name': 'test 01'
        }
        response2 = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_fund_enough_balance_diff_currency_should_return_400(self):
        transfer_data = {
            'from_jar': None,
            'to_jar': self.jar_eur_1.id,
            'currency': 'PLN',
            'amount': Decimal(10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_tranfer_fund_enough_balance_should_return_200(self):
        transfer_data = {
            'from_jar': self.jar_pln_2.id,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_tranfer_fund_not_enough_balance_should_return_400(self):
        transfer_data = {
            'from_jar': self.jar_pln_2.id,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(30),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_tranfer_fund_enough_balance_diff_currency_should_return_400(self):
        transfer_data = {
            'from_jar': self.jar_eur_1.id,
            'to_jar': self.jar_pln_1.id,
            'currency': 'PLN',
            'amount': Decimal(10),
            'name': 'test 01'
        }
        client = APIClient()
        response = client.post(reverse('api-transfer-funds'), transfer_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

