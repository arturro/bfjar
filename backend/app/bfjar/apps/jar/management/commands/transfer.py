import logging

from decimal import Decimal
from django.core.management.base import BaseCommand

from bfjar.apps.jar.models import Currency, Jar, Transfer


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('from_jar', nargs='?', default=None, type=int)
        parser.add_argument('to_jar', nargs='?', default=None, type=int)
        parser.add_argument('currency', nargs='?', default=None, type=str)
        parser.add_argument('amount', nargs='?', default=None, type=Decimal)
        parser.add_argument('name', nargs='?', default=None, type=str)
        pass

    def handle(self, *args, **options):
        from_jar = Jar.objects.get(pk=1)
        to_jar = Jar.objects.get(pk=2)
        currency = Currency.objects.get(pk=1)
        # transfer = Transfer.objects.transfer(to_jar, currency, Decimal(10), 'wplata')
        transfer = Transfer.objects.transfer(to_jar, currency, Decimal(10), 'przelew', from_jar)
        print(transfer)
        print('ok')
