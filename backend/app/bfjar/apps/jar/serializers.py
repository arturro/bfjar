from rest_framework import serializers

from .models import Currency, Jar, Transfer


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ['id', 'code']


class JarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Jar
        fields = ['id', 'currency', 'balance', 'created_date', 'name']


class TransferSerializer(serializers.ModelSerializer):
    # currency = CurrencySerializer() -
    """
    "currency": {
      "id": 1,
      "code": "PLN"
    },

    or
    "currency": 1,
    """

    class Meta:
        model = Transfer
        fields = ['id', 'from_jar', 'to_jar', 'currency', 'amount', 'created_date', 'name']


class TransferFundSerializer(serializers.Serializer):
    from_jar = serializers.IntegerField(min_value=0)
    to_jar = serializers.IntegerField(min_value=0)
    currency = serializers.CharField(max_length=3)
    amount = serializers.DecimalField(max_digits=6, decimal_places=2)
    name = serializers.CharField()
