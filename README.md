# README #

## Requirements ## 

* python3.7
* sqlite3

## Install ###

clone this repo:
$ git clone git@bitbucket.org:arturro/bfjar.git

from repo directory run:
$ pipenv install
$ pipenv shell
$ cd bfjar/backend/app

run server:
$ python manage.py runserver

run tests:
$python manage.py test

### Etap 1. Funkcjonalność słoika. ###

* Do słoika można włożyć środki. - done
```
http://127.0.0.1:8000/api/v1/transfer-funds/
{
    "from_jar": null,
    "to_jar": 1,
    "currency": "PLN",
    "amount": 10,
    "name": "003"
}
```

* Ze słoika można wyjąć środki. - done
```
http://127.0.0.1:8000/api/v1/transfer-funds/
{
    "from_jar": null,
    "to_jar": 1,
    "currency": "PLN",
    "amount": -10,
    "name": "003"
}
```

* Można przejrzeć historię operacji w słoiku (przynajmniej czas wykonania, tytuł i kwota). - done

http://127.0.0.1:8000/api/v1/transfer/?ordering=-amount,-created_date&from_jar=1

* Historię operacji można sortować (min. czas wykonania, kwota).

http://127.0.0.1:8000/api/v1/transfer/?ordering=-amount,-created_date&from_jar=1

### Etap 2. Funkcjonalność wielu słoików. ###

* Można utworzyć więcej niż jeden słoik. - done
```
http://127.0.0.1:8000/api/v1/jar/
{
    "currency": 1,
    "balance": 10,
    "name": "JAR 01"
}
``` 
* Środki można przenosić między słoikami. - done
```
http://127.0.0.1:8000/api/v1/transfer-funds/
{
    "from_jar": 2,
    "to_jar": 1,
    "currency": "PLN",
    "amount": -10,
    "name": "003"
}
```

* Można wyświetlić historię wszystkich operacji i filtrować wg. słoików. {{ host }}/api/v1/transfer/?ordering=--amount,-created_date&from_jar=1 - done

### Etap 3. Słoiki walutowe. ###

* Słoikowi i operacjom można przypisać walutę (jedną z kilku do wyboru). - done
* Do danego słoika wpłata może wpaść tylko jeśli waluta operacji odpowiada ustawionej dla słoika. - done

#### Wskazówki ####

* Wszystkie etapy nie są wymagane. Zrób tyle ile jesteś w stanie w wyznaczonym czasie. Lepiej zrobić dobrze jeden etap niż cztery byle jak.
* W przypadku wątpliwości co do szczegółów wymagań przyjmij rozwiązanie, które uważasz za najbardziej sensowne.
* Rodzaj interfejsu jest dowolny (REST API, CLI) REST
* Testy automatyczne. - TODO
* Mile widziane wykorzystanie dowolnej bazy danych i podział projektu na warstwy. - TODO sqlite3 - przeniesc na docker + postgresql
